extends CanvasLayer

var global_vars

func _ready():
	global_vars = get_node("/root/GlobalVariables")
	$AboutLabel.hide()
	$MainMenuContainer.hide()

func _process(delta):
	pass

func _input(event):
	if event is InputEventKey:
		if event.pressed:
			if event.scancode == KEY_ESCAPE && !$AboutLabel.is_visible_in_tree() && !global_vars.filedialog_open:
				if global_vars.is_menu_on:
					$MainMenuContainer.hide()
					get_tree().paused = false
				else:
					$MainMenuContainer.show()
					get_tree().paused = true

				global_vars.is_menu_on = !global_vars.is_menu_on


func _on_LoadGameButton_pressed():
	pass 


func _on_AboutButton_pressed():
	$MainMenuContainer.hide()
	$AboutLabel.show()


func _on_ExitButton_pressed():
	get_tree().quit()



func _on_BackButton_pressed():
	$AboutLabel.hide()
	$MainMenuContainer.show()
