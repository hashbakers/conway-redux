extends Node

var cam
var infobar
var cam_limit_top
var cam_limit_left
var cam_limit_right
var cam_limit_bottom
var global_vars
var size_x
var size_y
var window_focused = true

func _ready():
	cam = get_node("/root/Node2D/Camera2D")
	infobar = get_node("/root/Node2D/UI/InfoBar")
	global_vars = get_node("/root/GlobalVariables")
	size_x = get_node("/root/Node2D/TileMap").cell_size.x*global_vars.field_size_x
	size_y = get_node("/root/Node2D/TileMap").cell_size.y*global_vars.field_size_y
	cam.position = Vector2(size_x/2,size_y/2)
	cam_limit_left = -(size_x/8)
	cam_limit_top = -(size_y/8)
	cam_limit_right = size_x-(size_x/8)
	cam_limit_bottom = size_y-(size_y/8)

func _input(event):
	if global_vars.filedialog_open:
		return
	#keyboard
	if event is InputEventKey:
		if event.is_pressed():
			if event.scancode == KEY_LEFT and cam.position.x > cam_limit_left:
				cam.move_local_x(-5)
			if event.scancode == KEY_RIGHT and cam.position.x < cam_limit_right:
				cam.move_local_x(5)
			if event.scancode == KEY_UP and cam.position.y > cam_limit_top:
				cam.move_local_y(-5)
			if event.scancode == KEY_DOWN and cam.position.y < cam_limit_bottom:
				cam.move_local_y(5)
			if event.scancode == KEY_1:
				cam.zoom -= Vector2(0.25,0.25)
				infobar.add_info("Zoom: "+str(stepify(1/cam.zoom.x,0.01))+"x")
			if event.scancode == KEY_2:
				cam.zoom += Vector2(0.25,0.25)
				infobar.add_info("Zoom: "+str(stepify(1/cam.zoom.x,0.01))+"x")
	#viewport zoom via mouse
	if event is InputEventMouseButton:
		if event.pressed:
			if event.button_index == BUTTON_WHEEL_UP and cam.zoom > Vector2(0.5,0.5):
				var tween = Tween.new();
				tween.interpolate_property(cam, "zoom", cam.zoom, cam.zoom - Vector2(0.5,0.5), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
				add_child(tween)
				tween.start()
				var VPsize = cam.get_viewport().size*cam.zoom
				if VPsize.x > size_x:
					cam.position.x = (size_x/2)
				if VPsize.y > size_y:
					cam.position.y = (size_y/2)
			if event.button_index == BUTTON_WHEEL_DOWN:
				var tween = Tween.new();
				tween.interpolate_property(cam, "zoom", cam.zoom, cam.zoom + Vector2(0.5,0.5), 0.11, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
				add_child(tween)
				tween.start()
				var VPsize = cam.get_viewport().size*cam.zoom
				if VPsize.x > size_x:
					cam.position.x = (size_x/2)
				if VPsize.y > size_y:
					cam.position.y = (size_y/2)
func _process(delta):
	if global_vars.filedialog_open or !window_focused:
		return
	size_x = get_node("/root/Node2D/TileMap").cell_size.x*global_vars.field_size_x
	size_y = get_node("/root/Node2D/TileMap").cell_size.y*global_vars.field_size_y
	cam_limit_left = -(size_x/8)
	cam_limit_top = -(size_y/8)
	cam_limit_right = size_x-(size_x/8)
	cam_limit_bottom = size_y-(size_y/8)
	var VPsize = get_viewport().size
	var mouse_pos = get_viewport().get_mouse_position()
	if mouse_pos.x < VPsize.x/8 and cam.position.x > cam_limit_left:
		cam.move_local_x(-((VPsize.x/8)-mouse_pos.x)*1/4)
	if mouse_pos.x > VPsize.x*7/8 and cam.position.x < cam_limit_right:
		cam.move_local_x((mouse_pos.x-(VPsize.x*7/8))*1/4)
	if mouse_pos.y < VPsize.y/8 and cam.position.y > cam_limit_top:
		cam.move_local_y(-((VPsize.y/8)-mouse_pos.y)*1/4)
	if mouse_pos.y > VPsize.y*7/8 and cam.position.y < cam_limit_bottom:
		cam.move_local_y((mouse_pos.y-(VPsize.y*7/8))*1/4)

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_FOCUS_IN:
		window_focused = true
	elif what == MainLoop.NOTIFICATION_WM_FOCUS_OUT:
		window_focused = false
