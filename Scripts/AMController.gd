extends Node

var map
var infobar
var automata
var debugfieldbar
var current_automata = 0
var step_timer
var global_vars
var threading
var semaphore
var should_exit = false
var thread_count
var thread_ready = 0
var thread_ready_mutex
var processing_mutex
var all_thread_ready_semaphore
var action_lookup_list = {}
var global_add_list = []
var global_add_list_mutex
var global_remove_list = []
var global_remove_list_mutex

func alp_add_point(x,y):
	x = int(x)
	y = int(y)
	var key = [(x-(x%10))/10,(y-(y%10))/10]
	if key in action_lookup_list:
		action_lookup_list[key] += 1
	else:
		action_lookup_list[key] = 1
func alp_remove_point(x,y):
	x = int(x)
	y = int(y)
	var key = [(x-(x%10))/10,(y-(y%10))/10]
	if key in action_lookup_list:
		if action_lookup_list[key] <= 1:
			action_lookup_list.erase(key)
		else:
			action_lookup_list[key] -= 1

func _ready():
	map = get_node("/root/Node2D/TileMap")
	infobar = get_node("/root/Node2D/UI/InfoBar")
	global_vars = get_node("/root/GlobalVariables")
	automata = get_node("/root/Automata")
	debugfieldbar = get_node("/root/Node2D/UI/Fields")
	threading = OS.can_use_threads()
	if threading:
		semaphore = Semaphore.new()
		all_thread_ready_semaphore = Semaphore.new()
		global_add_list_mutex = Mutex.new()
		global_remove_list_mutex = Mutex.new()
		processing_mutex = Mutex.new()
		thread_ready_mutex = Mutex.new()
		thread_count = OS.get_processor_count()
		for i in range(0,thread_count):
			var thread = Thread.new()
			thread.start(self,"step_current_automata_thread",i)
			pass
	step_timer = Timer.new()
	step_timer.connect("timeout",self,"timer_timeout") 
	add_child(step_timer)
	
func check_zone_sides(zone):
	var return_val = 0
	var x = zone[0]
	var y = zone[1]
	#up
	for xc in range((x*10),(x*10)+10):
		var cell_val = map.get_cellv(Vector2(xc,y*10))
		if cell_val == 0:
			return_val += 1
			break
	#down
	for xc in range((x*10),(x*10)+10):
		var cell_val = map.get_cellv(Vector2(xc,(y*10)+9))
		if cell_val == 0:
			return_val += 2
			break
	#left
	for yc in range((y*10),(y*10)+10):
		var cell_val = map.get_cellv(Vector2(x*10,yc))
		if cell_val == 0:
			return_val += 4
			break
	#right
	for yc in range((y*10),(y*10)+10):
		var cell_val = map.get_cellv(Vector2((x*10)+9,yc))
		if cell_val == 0:
			return_val += 8
			break
	return return_val
	

func step_current_automata():
	if threading:
		step_current_automata_parallel()
	else:
		step_current_automata_linear()

func step_current_automata_linear():
	var point_backlog = []
	for y in range(0,global_vars.field_size_y):
		for x in range(0,global_vars.field_size_x):
			var cell = Vector2(x,y)
			if automata.step_cell_functions[current_automata].call_func(cell,map.get_cellv(cell)):
				point_backlog += [cell]
	map.clear()
	global_vars.iteration_count += 1
	for point in point_backlog:
		map.set_cellv(point,0)
	
func step_current_automata_parallel():
	for zone in action_lookup_list:
		var retval = check_zone_sides(zone)
		var play_area = Rect2(Vector2(0,0),Vector2(global_vars.field_size_x-1,global_vars.field_size_y-1))
		var check_zone
		if retval & 1:
			check_zone = [zone[0],zone[1]-1]
			if not action_lookup_list.has(check_zone) and play_area.has_point(Vector2(check_zone[0]*10,check_zone[1]*10)):
				action_lookup_list[check_zone] = 0
		if retval & 2:
			check_zone = [zone[0],zone[1]+1]
			if not action_lookup_list.has(check_zone) and play_area.has_point(Vector2(check_zone[0]*10,check_zone[1]*10)):
				action_lookup_list[check_zone] = 0
		if retval & 4:
			check_zone = [zone[0]-1,zone[1]]
			if not action_lookup_list.has(check_zone) and play_area.has_point(Vector2(check_zone[0]*10,check_zone[1]*10)):
				action_lookup_list[check_zone] = 0
		if retval & 8:
			check_zone = [zone[0]+1,zone[1]]
			if not action_lookup_list.has(check_zone) and play_area.has_point(Vector2(check_zone[0]*10,check_zone[1]*10)):
				action_lookup_list[check_zone] = 0
	if processing_mutex.try_lock() != OK:
		infobar.add_info("Simulation overrun - Skipping step")
		return
	for _i in range(0,thread_count):
		semaphore.post()
	all_thread_ready_semaphore.wait()
	for point in global_remove_list:
		map.set_cellv(point,-1)
		alp_remove_point(point.x,point.y)
	for point in global_add_list:
		map.set_cellv(point,0)
		alp_add_point(point.x,point.y)
	global_add_list = []
	global_remove_list = []
	thread_ready = 0
	for zone in action_lookup_list:
		if action_lookup_list[zone] == 0:
			action_lookup_list.erase(zone)
	global_vars.iteration_count += 1
	processing_mutex.unlock()
	print("-----------------------")
	
func step_current_automata_thread(thread_num):
	var mplr = 0
	var list_len
	var keys
	var key
	var cell
	var cell_val
	var eval_val
	var local_add_list = []
	var local_remove_list = []
	while !should_exit:
		semaphore.wait()
		list_len = len(action_lookup_list)
		keys = action_lookup_list.keys()
		while (thread_count*mplr)+thread_num < list_len:
			key = keys[(thread_count*mplr)+thread_num]
			for y in range((key[1]*10),(key[1]*10)+10):
				for x in range((key[0]*10),(key[0]*10)+10):
					cell = Vector2(x,y)
					cell_val = map.get_cellv(cell)
					eval_val = automata.step_cell_functions[current_automata].call_func(cell,cell_val)
					if cell_val == 0 and eval_val == false:
						local_remove_list += [cell]
					elif cell_val == -1 and eval_val == true:
						local_add_list += [cell]
			mplr += 1
		global_add_list_mutex.lock()
		for elem in local_add_list:
			if !global_add_list.has(elem):
				global_add_list.append(elem)
		global_add_list_mutex.unlock()
		global_remove_list_mutex.lock()
		for elem in local_remove_list:
			if !global_remove_list.has(elem):
				global_remove_list.append(elem)
		global_remove_list_mutex.unlock()
		thread_ready_mutex.lock()
		thread_ready += 1
		if thread_ready == thread_count:
			all_thread_ready_semaphore.post()
		thread_ready_mutex.unlock()
		print("Thread ",thread_num," finished processing! ",mplr," processed!")
		local_add_list = []
		local_remove_list = []
		mplr = 0
	print("Thread ",thread_num," quitting!")
func timer_timeout():
	step_current_automata()
	debugfieldbar.set_text("Fields: "+str(len(action_lookup_list))+"\nThreads: "+str(thread_count))
	
func _input(event):
	if global_vars.filedialog_open:
		return
	#keyboard
	if event is InputEventKey:
		if event.is_pressed():
			# step
			if event.scancode == KEY_S:
				# can't step if simulation is running!!!!
				if step_timer.is_stopped():
					step_current_automata()
					infobar.add_info("Step")
				else:
					infobar.add_info("Can't step while simulation is running!")
			# next simulation type
			if event.scancode == KEY_N:
				if step_timer.is_stopped():
					if current_automata == len(automata.step_cell_functions)-1:
						current_automata = 0
					else:
						current_automata += 1
					infobar.add_info("Simulation type switched to "+automata.step_cell_functions_name[current_automata])
					global_vars.iteration_count = 0
				else:
					infobar.add_info("Can't switch simulation type while simulation is running!")
			# start/stop simulation
			if event.scancode == KEY_SPACE:
				if step_timer.is_stopped():
					step_timer.start(global_vars.sim_step_time)
					infobar.add_info("Started simulation!")
				else:
					step_timer.stop()
					infobar.add_info("Stopped simulation!")
			if (event.scancode == KEY_PLUS or event.scancode == KEY_EQUAL) and global_vars.sim_step_time > 0.15:
				global_vars.sim_step_time -= 0.05
				if not step_timer.is_stopped():
					step_timer.stop()
					step_timer.start(global_vars.sim_step_time)
				infobar.add_info("Simulation step time: "+str(global_vars.sim_step_time)+" s")
			if event.scancode == KEY_MINUS and global_vars.sim_step_time <= 4.95:
				global_vars.sim_step_time += 0.05
				if not step_timer.is_stopped():
					step_timer.stop()
					step_timer.start(global_vars.sim_step_time)
				infobar.add_info("Simulation step time: "+str(global_vars.sim_step_time)+" s")
				
func _exit_tree():
	should_exit = true
	if(threading):
		for _i in range(0,thread_count):
			semaphore.post()
			
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		#on quitting - kill threads
		should_exit = true
		if(threading):
			for _i in range(0,thread_count):
				semaphore.post()
