extends Node

var map
var cam
var global_vars
var dialog
var proper_cam
var infobar
var amcontroller
var automata
var itercounter

func _ready():
	map = get_node("/root/Node2D/TileMap")
	cam = get_node("/root/Node2D/Camera2D")
	global_vars = get_node("/root/GlobalVariables")
	dialog = get_node("/root/Node2D/FileDialog")
	proper_cam = get_node("/root/Node2D/FileDialogCamera")
	infobar = get_node("/root/Node2D/UI/InfoBar")
	itercounter = get_node("/root/Node2D/UI/Label")
	amcontroller = get_node("/root/Node2D/AutomataController")
	automata = get_node("/root/Automata")
	#connect signals to things
	get_tree().get_root().connect("size_changed", self, "filedialog_resize")
	dialog.get_cancel().connect("pressed",self,"cleanup_connect")
	
func filedialog_resize():
	dialog.set_size(proper_cam.get_viewport().get_visible_rect().size)

func save_settings(path):
	var settings = {
		"field_size" : {
			"x" : global_vars.field_size_x,
			"y" : global_vars.field_size_y
		},
		"points" : [],
		"simulation_type" : automata.step_cell_functions_name[amcontroller.current_automata],
		"version" : global_vars.version
	}
	for point in map.get_used_cells():
		settings["points"].append([point.x,point.y])
	var file = File.new()
	file.open(path, File.WRITE)
	file.store_line(to_json(settings))
	file.close()
	infobar.add_info("Game saved!")

func load_settings(path):
	var file = File.new()
	file.open(path, File.READ)
	if path.get_extension() == "crsvgf":
		var settings = parse_json(file.get_as_text())
		file.close()
		if settings['version'] > global_vars.version:
			infobar.add_info("The file is incompatible with the current version!")
			return
		map.clear()
		if amcontroller.threading:
			amcontroller.global_add_list = []
			amcontroller.global_remove_list = []
			amcontroller.action_lookup_list = {}
		global_vars.field_size_x = settings['field_size']['x']
		global_vars.field_size_y = settings['field_size']['y']
		amcontroller.current_automata = automata.step_cell_functions_name.find(settings['simulation_type'])
		for point in settings['points']:
			map.set_cellv(Vector2(point[0],point[1]),0)
			amcontroller.alp_add_point(point[0],point[1])
	elif path.get_extension() == "rle":
		map.clear()
		if amcontroller.threading:
			amcontroller.global_add_list = []
			amcontroller.global_remove_list = []
			amcontroller.action_lookup_list = {}
		var start_x = -1
		var start_y = -1
		while not file.eof_reached():
			var line = file.get_line()
			if len(line) == 0:
				break
			if line[0] == "#":
				continue
			var regex = RegEx.new()
			regex.compile("x\\s*=\\s*(\\d+),\\s*y\\s*=\\s*(\\d+),\\s*rule\\s*=\\s*(.+)")
			var result = regex.search(line)
			if result:
				print(result.strings)
				global_vars.field_size_x = 5*int(result.strings[1])
				global_vars.field_size_y = 5*int(result.strings[2])
				amcontroller.current_automata = automata.rulestring_to_id(result.strings[3])
				start_x = (global_vars.field_size_x/2)-(int(result.strings[1])/2)
				start_y = (global_vars.field_size_y/2)-(int(result.strings[2])/2)
			else:
				var multiplier = ""
				var offset = 0
				for rune in line:
					print(rune)
					if rune == "b":
						if multiplier != "":
							offset += int(multiplier)
						else:
							offset += 1
						multiplier = ""
					elif rune == "o":
						var count = 0
						if multiplier != "":
							count = int(multiplier)
						else:
							count = 1
						for _i in range(0,count):
							print("Setting",start_x+offset,start_y)
							map.set_cellv(Vector2(start_x+offset,start_y),0)
							amcontroller.alp_add_point(start_x+offset,start_y)
							offset += 1
						multiplier = ""
					elif rune == "$":
						multiplier = ""
						offset = 0
						start_y += 1
					elif rune == "!":
						break
					else:
						multiplier += rune
		file.close()
	cam.position = Vector2(get_node("/root/Node2D/TileMap").cell_size.x*global_vars.field_size_x/2,get_node("/root/Node2D/TileMap").cell_size.y*global_vars.field_size_y/2)
	infobar.add_info("Game restored!")
		
func save_connect(path):
	dialog.disconnect("file_selected",self,"save_connect")
	save_settings(path)
	cleanup_connect()

func load_connect(path):
	dialog.disconnect("file_selected",self,"load_connect")
	load_settings(path)
	cleanup_connect()

func cleanup_connect():
	global_vars.filedialog_open = false
	infobar.show()
	itercounter.show()
	cam.make_current()

func _input(event):
	if event is InputEventKey:
		if event.pressed:
			if event.scancode == KEY_ESCAPE and global_vars.filedialog_open and !global_vars.is_menu_on:
				cleanup_connect()
				dialog.hide()
			if (event.scancode == KEY_I or event.scancode == KEY_E) and !global_vars.filedialog_open and amcontroller.step_timer.is_stopped():
				proper_cam.make_current()
				global_vars.filedialog_open = true
				infobar.hide()
				itercounter.hide()
				# import
				if event.scancode == KEY_I or global_vars.is_load_on:
					dialog.mode = FileDialog.MODE_OPEN_FILE
					var filters = dialog.get_filters()
					if filters.size() == 1:
						filters.append("*.rle; Run Length Encoded")
					dialog.set_filters(filters)
					dialog.connect("file_selected",self,"load_connect")
					global_vars.is_load_on = false
				#export
				elif event.scancode == KEY_E:
					dialog.mode = FileDialog.MODE_SAVE_FILE
					var filters = dialog.get_filters()
					if filters.size() > 1:
						filters.remove(1)
						dialog.set_filters(filters)
					dialog.connect("file_selected",self,"save_connect")
				dialog.popup(proper_cam.get_viewport().get_visible_rect())
