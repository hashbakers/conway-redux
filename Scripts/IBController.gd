extends Node

var global_vars

func _ready():
	global_vars = get_node("/root/GlobalVariables")

func add_info(text):
	if self.text.empty():
		self.text += text
	else:
		self.text += "\n"+text
	var timer = Timer.new()
	timer.connect("timeout",self,"info_timeout") 
	timer.set_one_shot(true)
	add_child(timer)
	timer.start(global_vars.infomsg_timeout)

func info_timeout():
	if self.text.find("\n") == -1:
		self.text = ""
	else:
		self.text = self.text.right(self.text.find("\n")+1)
