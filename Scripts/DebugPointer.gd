extends Label

var map
var cam

func _ready():
	map = get_node("/root/Node2D/TileMap")
	cam = get_node("/root/Node2D/Camera2D")

func _process(delta: float) -> void:
	set_text(str(map.world_to_map(((get_viewport().get_mouse_position()-(cam.get_viewport().size/2))*cam.zoom)+cam.position)))
	
