extends Node2D

var map
var cam
var global_vars
var infobar
var ammap

func _ready():
	map = get_node("/root/Node2D/TileMap")
	cam = get_node("/root/Node2D/Camera2D")
	global_vars = get_node("/root/GlobalVariables")
	infobar = get_node("/root/Node2D/UI/InfoBar")
	ammap = get_node("/root/Node2D/AutomataController")
	
func _input(event):
	if global_vars.filedialog_open:
		return
	if event is InputEventKey:
		if event.pressed:
			if event.scancode == KEY_C:
				if(ammap.threading):
					ammap.action_lookup_list = {}
				global_vars.iteration_count = 0
				map.clear()
				infobar.add_info("Cleared play area!")
	if event is InputEventMouseButton:
		if event.pressed:
			if event.button_index == BUTTON_LEFT:
				var map_pos = map.world_to_map(((event.position-(cam.get_viewport().size/2))*cam.zoom)+cam.position)
				if map_pos.x < 0 or map_pos.x > (global_vars.field_size_x-1) or map_pos.y < 0 or map_pos.y > (global_vars.field_size_y-1):
					return
				global_vars.iteration_count = 0
				if map.get_cellv(map_pos) == -1:
					map.set_cellv(map_pos,0)
					ammap.alp_add_point(map_pos.x,map_pos.y)
				else:
					map.set_cellv(map_pos,-1)
					ammap.alp_remove_point(map_pos.x,map_pos.y)
			
func _draw():
	var COLUMNS = global_vars.field_size_x
	var ROWS = global_vars.field_size_y
	var CEL_SIZE = 128
	
	for x in range (COLUMNS+1):
		draw_line(Vector2(x*CEL_SIZE,0),Vector2(x*CEL_SIZE,ROWS*CEL_SIZE),"#AAAAAA",0.5)
	for y in range (ROWS+1):
		draw_line(Vector2(0,y*CEL_SIZE),Vector2(COLUMNS*CEL_SIZE,CEL_SIZE*y),"#AAAAAA",0.5)	
