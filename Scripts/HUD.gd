extends CanvasLayer

var is_menu_on = true
var global_vars

func _ready():
	global_vars = get_node("/root/GlobalVariables")
	$AboutLabel.hide()
	$NewGameLabel.hide()

func _process(delta):
	pass

func _on_NewGameButton_pressed():
	is_menu_on = false
	$MainMenuContainer.hide()
	$NewGameLabel.show()
	$NewGameLabel/HeightLineEdit.text = str(global_vars.field_size_x)
	$NewGameLabel/WidthLineEdit.text = str(global_vars.field_size_y)
	
	if is_valid_input($NewGameLabel/HeightLineEdit.text) && is_valid_input($NewGameLabel/WidthLineEdit.text):
		$NewGameLabel/OkButton.show()
	else:
		$NewGameLabel/OkButton.hide()
	
	
func _on_LoadGameButton_pressed():
	global_vars.is_load_on = true


func _on_AboutButton_pressed():
	$MainMenuContainer.hide()
	$AboutLabel.show()


func _on_ExitButton_pressed():
	get_tree().quit()


func _on_BackButton_pressed():
	$AboutLabel.hide()
	$MainMenuContainer.show()


func _on_NewGameBackButton_pressed():
	$NewGameLabel.hide()
	$MainMenuContainer.show()

func _on_OkButton_pressed():
	get_tree().change_scene("res://Scenes/GameScene.tscn")

func _on_HeightLineEdit_text_changed(new_text):
	if is_valid_input(new_text):
		global_vars.field_size_x = int(new_text)
	else:
		$NewGameLabel/OkButton.hide()
	if is_valid_input(new_text) and is_valid_input($NewGameLabel/WidthLineEdit.text):
		$NewGameLabel/OkButton.show()

func _on_WidthLineEdit_text_changed(new_text):
	if is_valid_input(new_text):
		global_vars.field_size_y = int(new_text)
	else:
		$NewGameLabel/OkButton.hide()
	if is_valid_input(new_text) and is_valid_input($NewGameLabel/HeightLineEdit.text):
		$NewGameLabel/OkButton.show()

func is_valid_input(input):
	if len(input) > 0 and is_representing_int(input):
		return true
	
	return false
	
func is_representing_int(number):
	if int(number) > 0:
		return true
	else:
		return false
