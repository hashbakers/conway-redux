extends Node

# constants
const version = 2

# variables
var field_size_x = 50 # tilemap size x
var field_size_y = 50 # tilemap size y
var sim_step_time = 0.5 # time between simulation steps
var infomsg_timeout = 3 # infobar message ttl
var filedialog_open = false # is the filedialog open?
var iteration_count = 0 # num of simulation iterations done
var is_menu_on = false
var is_about_on = false
var is_load_on = false
