extends Node

var cam
var global_vars
var automata
var amcontroller

func _ready():
	amcontroller = get_node("/root/Node2D/AutomataController")
	automata = get_node("/root/Automata")
	cam = get_node("/root/Node2D/Camera2D")
	global_vars = get_node("/root/GlobalVariables")
	pass 

func _process(delta):
	var vp = cam.get_viewport().get_visible_rect().size
	self.rect_position = Vector2(vp.x-self.rect_size.x-10,10)
	self.text = str(global_vars.iteration_count)+" :ITER\n"+automata.step_cell_functions_name[amcontroller.current_automata]+": TYPE"
