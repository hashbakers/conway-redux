extends Node2D

var _map
var step_cell_functions = [funcref(self,"cell_conway_gol"),funcref(self,"cell_highlife"),funcref(self,"cell_lfod"),funcref(self,"cell_daynight"),funcref(self,"cell_2by2"),funcref(self,"cell_diamoeba"),funcref(self,"cell_gems")]
var step_cell_functions_name = ["Conway's Game of Life","HighLife","Live Free or Die","Day & Night","2x2","Diamoeba","Gems"]

func _ready():
	_map = get_node("/root/Node2D/TileMap")

func rulestring_to_id(rulestring):
	if rulestring == "23/3" or rulestring == "B3/S23":
		return 0
	elif rulestring == "23/36" or rulestring == "B36/S23":
		return 1
	elif rulestring == "0/2" or rulestring == "B2/S0":
		return 2
	elif rulestring == "34678/3678" or rulestring == "B3678/S34678":
		return 3
	elif rulestring == "125/36" or rulestring == "B36/S125":
		return 4
	elif rulestring == "5678/35678" or rulestring == "B35678/S5678":
		return 5
	elif rulestring == "4568/3457" or rulestring == "B3457/S4568":
		return 6

func count_moore_neighbours(point):
	if _map == null:
		_map = get_node("/root/Node2D/TileMap")
	var neighbors = 0
	neighbors += 1 + _map.get_cellv(point+Vector2(-1,0))
	neighbors += 1 + _map.get_cellv(point+Vector2(-1,-1))
	neighbors += 1 + _map.get_cellv(point+Vector2(0,-1))
	neighbors += 1 + _map.get_cellv(point+Vector2(1,-1))
	neighbors += 1 + _map.get_cellv(point+Vector2(1,0))
	neighbors += 1 + _map.get_cellv(point+Vector2(1,1))
	neighbors += 1 + _map.get_cellv(point+Vector2(0,1))
	neighbors += 1 + _map.get_cellv(point+Vector2(-1,1))
	return neighbors

# automata implementations
func cell_conway_gol(pos,cell_value):
	var cell_neighbors = count_moore_neighbours(pos)
	if (cell_neighbors == 2 and cell_value == 0) or cell_neighbors == 3:
		return true
	else:
		return false

func cell_highlife(pos,cell_value):
	var cell_neighbors = count_moore_neighbours(pos)
	if (cell_neighbors == 2 and cell_value == 0) or cell_neighbors == 3 or (cell_neighbors == 6 and cell_value == -1):
		return true
	else:
		return false
		
func cell_lfod(pos,cell_value):
	var cell_neighbors = count_moore_neighbours(pos)
	if (cell_neighbors == 0 and cell_value == 0) or (cell_neighbors == 2 and cell_value == -1):
		return true
	else:
		return false

func cell_daynight(pos,cell_value):
	var cell_neighbors = count_moore_neighbours(pos)
	if (cell_neighbors == 4 and cell_value == 0) or (cell_neighbors == 8 and cell_value == -1) or cell_neighbors == 3 or cell_neighbors == 6 or cell_neighbors == 7:
		return true
	else:
		return false

func cell_2by2(pos,cell_value):
	var cell_neighbors = count_moore_neighbours(pos)
	if ((cell_neighbors == 1 or cell_neighbors == 2 or cell_neighbors == 5) and cell_value == 0) or ((cell_neighbors == 3 or cell_neighbors == 6) and cell_value == -1):
		return true
	else:
		return false

func cell_diamoeba(pos,cell_value):
	var cell_neighbors = count_moore_neighbours(pos)
	if cell_neighbors > 4 or (cell_neighbors == 3 and cell_value == -1):
		return true
	else:
		return false
		
func cell_gems(pos,cell_value):
	var cell_neighbors = count_moore_neighbours(pos)
	if ((cell_neighbors == 6 or cell_neighbors == 8) and cell_value == 0) or ((cell_neighbors == 3 or cell_neighbors == 7) and cell_value == -1) or cell_neighbors == 4 or cell_neighbors == 7:
		return true
	else:
		return false
