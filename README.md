# Conway Redux

Fancy cellular automata

# Controls
Move with mouse, place cells with LMB, zoom with scroll wheel.

| Keyboard | Action |
| ------ | ------ |
| space | start/stop simulation |
| - | slow down simulation |
| +/= | speed up simulation |
| s | step simulation |
| n | next cellular automata |
| c | clear playfield |
| i | import game |
| e | export game |
